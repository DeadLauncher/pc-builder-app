const fetch = require("node-fetch");
const Bluebird = require("bluebird");

Promise = Bluebird;
fetch.Promise = Bluebird;

const express = require("express");

const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const { logger } = require("../internal/logger");
const mongoConnect = require("../internal/db/mongo").connect;

const config = require("../config");

const { applyRouter } = require("../routes/router");

const run = async () => {
  await mongoConnect();
  const { port } = config.api;
  const app = express();

  applyRouter(app);

  app.listen(port, function () {
    logger.log({ level: "info", message: `Start server on port ${port}` });
  });

  return app;
};
module.exports = {
  run,
};
