const { handleError } = require("../api");
const { withTransaction } = require("../with-transaction");

const wrapAsyncTransactionErrorMiddleware = (middleware) => async (
  req,
  res,
  next
) => {
  try {
    await withTransaction(async (session) => {
      await middleware(req, res, session);
    })();
  } catch (e) {
    handleError(res, e, next);
  }
};

module.exports = wrapAsyncTransactionErrorMiddleware;
