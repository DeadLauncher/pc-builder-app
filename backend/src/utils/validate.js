const { validationResult } = require("express-validator");
const { StatusCodes } = require("http-status-codes");
const {
  formatValidatorError,
  formatResponse,
  RESPONSE_STATUS_FAILED,
} = require("./api");

const validate = async (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    next();
    return;
  }
  const validationErrors = errors
    .array()
    .map((e) => formatValidatorError({ ...e, textCode: "validationError" }));
  const response = formatResponse(RESPONSE_STATUS_FAILED, {}, validationErrors);
  res.status(StatusCodes.BAD_REQUEST).json(response);
};

module.exports = validate;
