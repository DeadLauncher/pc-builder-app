module.exports = {
  name: {
    exists: { errorMessage: "Name is required" },
    trim: true,
  },
  brand: {
    exists: { errorMessage: "Brand is required" },
    trim: true,
  },
  price: {
    exists: { errorMessage: "Price is required" },
    isInt: {
      errorMessage: "Wrong price",
      options: { gt: 0 },
    },
  },
  weight: {
    exists: { errorMessage: "Weight is required" },
    isInt: {
      errorMessage: "Wrong weight",
      options: { gt: 0 },
    },
  },
  amount: {
    optional: {
      isInt: { errorMessage: "Wrong amount" },
    },
  },
};
