const mongoose = require("mongoose");

const withTransaction = (func, onRollback) => async (...args) => {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const response = await func(...args, session);
    await session.commitTransaction();
    return response;
  } catch (e) {
    await session.abortTransaction();
    if (onRollback) await onRollback(...args);
    throw e;
  } finally {
    session.endSession();
  }
};

module.exports = { withTransaction };
