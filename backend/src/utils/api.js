const { StatusCodes } = require("http-status-codes");

const RESPONSE_STATUS_SUCCESS = "success";
const RESPONSE_STATUS_FAILED = "failed";

const formatResponse = (status, result = {}, errors = []) => ({
  status,
  result,
  errors,
});

const formatError = (message, textCode, param, location) => ({
  message,
  textCode,
  param,
  location,
});

const formatValidatorError = (error) =>
  formatError(error.msg, error.textCode, error.param, error.location);

const sendInternalServerError = (res, e, next) => {
  const error = formatError("Internal server error");
  const response = formatResponse(RESPONSE_STATUS_FAILED, {}, [error]);
  res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(response);
  next(e);
};

class ApiError extends Error {
  constructor({ statusCode, message, textCode, params, location }) {
    super();
    this.message = message;
    this.statusCode = statusCode;
    this.textCode = textCode;
    this.params = params;
    this.location = location;
  }
}

const handleError = (res, e, next) => {
  if (e instanceof ApiError) {
    const error = formatError(e.message, e.textCode, e.params, e.location);
    const response = formatResponse(RESPONSE_STATUS_FAILED, {}, [error]);
    res.status(e.statusCode).send(response);
    return;
  }
  if (next) {
    sendInternalServerError(res, e, next);
  } else {
    res.status(StatusCodes.NOT_FOUND).end();
  }
};

const sendResponse = (res, status, payload) => {
  const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, payload);
  res.status(status).json(formatedResult);
};

module.exports = {
  RESPONSE_STATUS_SUCCESS,
  RESPONSE_STATUS_FAILED,
  formatResponse,
  formatValidatorError,
  formatError,
  handleError,
  sendInternalServerError,
  sendResponse,
  ApiError,
};
