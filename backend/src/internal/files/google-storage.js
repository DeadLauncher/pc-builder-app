const { Storage } = require("@google-cloud/storage");
const { v4: uuidv4 } = require("uuid");
const fetch = require("node-fetch");
const config = require("../../config");
const { WrongFileFormatError } = require("../errors/files");

const storage = new Storage({
  keyFilename: "key.json",
});
const bucket = storage.bucket(config.google.storage.bucket);

const upload = async ({ name, contentType, buffer }) => {
  const blob = bucket.file(name);

  const options = {
    version: "v4",
    action: "write",
    expires: Date.now() + config.google.storage.timeout,
    contentType,
  };

  const [link] = await blob.getSignedUrl(options);

  await fetch(link, {
    method: "PUT",
    headers: {
      "Content-Type": contentType,
    },
    body: buffer,
  });

  return `/${config.google.storage.bucket}/${name}`;
};

const uploadFile = async (location, file, type, format) => {
  const { mimetype } = file;
  const [fileType, fileFormat] = mimetype.split("/");

  const wrongType = type && type !== fileType;
  const wrongFormat = format && format !== fileFormat;
  if (wrongType || wrongFormat) {
    throw new WrongFileFormatError();
  }

  const key = uuidv4();

  const params = {
    name: `${location}/${key}.${fileFormat}`,
    buffer: file.buffer,
    contentType: mimetype,
  };

  const result = await upload(params);
  return result;
};

const deleteFileByUrl = async (path) => {
  const [, , location, name] = path.split("/");
  const file = bucket.file(`${location}/${name}`);
  await file.delete();
};

module.exports = {
  deleteFileByUrl,
  uploadFile,
};
