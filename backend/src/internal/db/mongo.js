const mongoose = require("mongoose");
const config = require("../../config");
const { logger } = require("../logger");

mongoose.Promise = global.Promise;

const connect = async () => {
  const { url } = config.mongo;
  await mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
  logger.log({ level: "info", message: `Mongo connected to ${url}` });
};

module.exports = { connect };
