const Motherboard = require("./motherboard.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    formfactor,
    cpu_socket,
    memory_type,
    memory_slots,
    pci_slots,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [motherboard] = await Motherboard.create(
    [
      {
        name,
        brand,
        price,
        weight,
        formfactor,
        cpu_socket,
        memory_type,
        memory_slots,
        pci_slots,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return motherboard;
};

module.exports = { insert };
