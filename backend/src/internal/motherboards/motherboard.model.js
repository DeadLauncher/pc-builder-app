const mongoose = require("mongoose");
const { logger } = require("../logger");

const Motherboard = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  formfactor: { type: String },
  cpuSocket: { type: String },
  memoryType: { type: String },
  memorySlots: { type: Number },
  pciSlots: { type: Number },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Motherboard.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    cpuSocket: this.cpuSocket,
    memoryType: this.memoryType,
    memorySlots: this.memorySlots,
    pciSlots: this.pciSlots,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Motherboard", Motherboard);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
