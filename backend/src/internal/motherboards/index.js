const { getMotherboards } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(motherboard) {
  return await motherboard.toDTO();
}

async function serializeMany(motherboards) {
  return await Promise.all(
    motherboards.map(async function (ps) {
      return await serialize(ps);
    })
  );
}

module.exports = {
  getMotherboards,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
