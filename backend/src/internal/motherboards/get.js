const Motherboard = require("./motherboard.model");

async function getMotherboards() {
  return await Motherboard.find({});
}

module.exports = { getMotherboards };
