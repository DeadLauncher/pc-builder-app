const Build = require("./build.model");

async function getBuilds() {
  return await Build.find({});
}

module.exports = { getBuilds };
