const { getBuilds } = require("./get");

async function serialize(build) {
  return await build.toDTO();
}

async function serializeMany(builds) {
  return await Promise.all(
    builds.map(async function (c) {
      return await serialize(c);
    })
  );
}

module.exports = { getBuilds, serialize, serializeMany };
