const mongoose = require("mongoose");
const { logger } = require("../logger");

const { ObjectId } = mongoose.Types;

const Build = mongoose.Schema({
  name: { type: String },
  cpu: { type: ObjectId, ref: "Cpu" },
  storage: { type: ObjectId, ref: "Storage" },
  casee: { type: ObjectId, ref: "Case" },
  motherboard: { type: ObjectId, ref: "Motherboard" },
  powersupply: { type: ObjectId, ref: "Powersupply" },
  gpu: { type: ObjectId, ref: "Gpu" },
  memory: { type: ObjectId, ref: "Memory" },
});

Build.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    cpu,
    storage,
    casee,
    motherboard,
    powersupply,
    gpu,
    memory,
  };
};

const model = mongoose.model("Build", Build);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
