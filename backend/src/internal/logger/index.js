const winston = require("winston");
const expressWinston = require("express-winston");

const config = require("../../config");

const transportConsole = new winston.transports.Console({
  silent: !config.logger.enabled,
});

const logger = winston.createLogger({
  transports: [transportConsole],
  level: "info",
  format: winston.format.json(),
});

const expressRequestLogger = expressWinston.logger({
  transports: [transportConsole],
  format: winston.format.json(),
  msg: "HTTP {{req.url}} {{res.statusCode}} {{req.method}} {{res.responseTime}}ms",
  expressFormat: true,
  colorize: false,
});

const expressErrorLogger = expressWinston.errorLogger({
  transports: [transportConsole],
  format: winston.format.json(),
});

module.exports = {
  logger,
  expressRequestLogger,
  expressErrorLogger,
};
