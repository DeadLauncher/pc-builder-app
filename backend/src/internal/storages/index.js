const { getStorages } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(storage) {
  return await storage.toDTO();
}

async function serializeMany(storages) {
  return await Promise.all(
    storages.map(async function (ps) {
      return await serialize(ps);
    })
  );
}

module.exports = {
  getStorages,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
