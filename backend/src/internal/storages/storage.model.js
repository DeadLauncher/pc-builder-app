const mongoose = require("mongoose");
const { logger } = require("../logger");

const Storage = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  type: { type: String },
  capacity: { type: Number },
  spindleSpeed: { type: Number },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Storage.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    type: this.type,
    capacity: this.capacity,
    spindleSpeed: this.spindleSpeed,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Storage", Storage);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
