const Storage = require("./storage.model");

async function getStorages() {
  return await Storage.find({});
}

module.exports = { getStorages };
