const Storage = require("./storage.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    type,
    capacity,
    spindleSpeed,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [storage] = await Storage.create(
    [
      {
        name,
        brand,
        price,
        weight,
        type,
        capacity,
        spindleSpeed,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return storage;
};

module.exports = { insert };
