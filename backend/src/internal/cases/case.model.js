const mongoose = require("mongoose");
const { logger } = require("../logger");

const Case = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  size: { type: String },
  mbFormfactor: [{ type: String }],
  psFormfactor: [{ type: String }],
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Case.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    size: this.size,
    mbFormfactor: this.mbFormfactor,
    psFormfactor: this.psFormfactor,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Case", Case);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
