const Case = require("./case.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    size,
    mbFormfactor,
    psFormfactor,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [casee] = await Case.create(
    [
      {
        name,
        brand,
        price,
        weight,
        size,
        mbFormfactor,
        psFormfactor,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return casee;
};

module.exports = { insert };
