module.exports = {
  FullTower: "FULLTOWER",
  MidTower: "MIDTOWER",
  MiniTower: "MINITOWER",
  Compact: "COMPACT",
};
