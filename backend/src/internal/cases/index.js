const { getCases } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(casee) {
  return await casee.toDTO();
}

async function serializeMany(cases) {
  return await Promise.all(
    cases.map(async function (c) {
      return await serialize(c);
    })
  );
}

module.exports = {
  getCases,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
