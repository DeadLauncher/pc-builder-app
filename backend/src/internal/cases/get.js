const Case = require("./case.model");

async function getCases() {
  return await Case.find({});
}

module.exports = { getCases };
