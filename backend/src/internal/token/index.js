const jwt = require("jsonwebtoken");
const config = require("../../config");

const generateAccessToken = (user) => {
  const { id, sessionKey } = user;
  return jwt.sign({ id, sessionKey }, config.users.auth.jwtSecret, {
    expiresIn: config.users.auth.accessTokenMaxAge,
  });
};

module.exports = { generateAccessToken };
