const { StatusCodes } = require("http-status-codes");
const { ApiError } = require("../../utils/api");

/**
 * @apiDefine UserIsNotRegisteredError
 *
 * @apiErrorExample UserIsNotRegisteredError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *         "statusCode": 403,
 *          "message": "User is not registered yet",
 *          "textCode": "userIsNotRegistered"
 *        }]
 *     }
 */
class UserIsNotRegisteredError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "User is not registered yet",
      textCode: "userIsNotRegistered",
    });
  }
}

/**
 * @apiDefine WrongTokenError
 *
 * @apiErrorExample WrongTokenError:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 401,
 *          "message": "Wrong token",
 *          "textCode": "WrongToken",
 *          "param": "accessToken",
 *          "location": "authorization/header"
 *        }]
 *     }
 */
class WrongTokenError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.UNAUTHORIZED,
      message: "Wrong token",
      textCode: "WrongToken",
      param: "accessToken",
      location: "authorization/header",
    });
  }
}

/**
 * @apiDefine EmailOrPhoneNotVerifiedError
 *
 * @apiErrorExample EmailOrPhoneNotVerifiedError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *         "statusCode": 403,
 *          "message": "Email or phone not verified",
 *          "textCode": "emailOrPhoneNotVerified"
 *        }]
 *     }
 */
class EmailOrPhoneNotVerifiedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Email or phone not verified",
      textCode: "emailOrPhoneNotVerified",
    });
  }
}

/**
 * @apiDefine UserDoesntHaveEnoughPermissionsError
 *
 * @apiErrorExample UserDoesntHaveEnoughPermissionsError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *         "statusCode": 403,
 *          "message": "Not enough permissions",
 *          "textCode": "userDoesntHaveEnoughPermissions"
 *        }]
 *     }
 */
class UserDoesntHaveEnoughPermissionsError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Not enough permissions",
      textCode: "userDoesntHaveEnoughPermissions",
    });
  }
}

/**
 * @apiDefine AuthMiddlewareError
 *
 * @apiErrorExample AuthMiddlewareError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *         "statusCode": 403,
 *          "message": "Use auth middleware before this",
 *          "textCode": "authMiddleware"
 *        }]
 *     }
 */
class AuthMiddlewareError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Use auth middleware before this",
      textCode: "authMiddleware",
    });
  }
}

module.exports = {
  UserIsNotRegisteredError,
  WrongTokenError,
  EmailOrPhoneNotVerifiedError,
  UserDoesntHaveEnoughPermissionsError,
  AuthMiddlewareError,
};
