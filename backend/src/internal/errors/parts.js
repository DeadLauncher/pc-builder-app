const { StatusCodes } = require("http-status-codes");
const { ApiError } = require("../../utils/api");

class PartImageIsRequiredError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Part image is required",
      textCode: "PartImageIsRequired",
    });
  }
}

module.exports = { PartImageIsRequiredError };
