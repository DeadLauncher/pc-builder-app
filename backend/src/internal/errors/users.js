const { StatusCodes } = require("http-status-codes");
const { ApiError } = require("../../utils/api");

/**
 * @apiDefine CooldownError
 *
 * @apiErrorExample CooldownError:
 *   HTTP/1.1 429 TooManyRequests
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 429,
 *        "message": "Cooldown hasn't passed yet",
 *        "textCode": "cooldownHasntPassedYet"
 *     }],
 *   }
 */
class CooldownError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.TOO_MANY_REQUESTS,
      message: "Cooldown hasn't passed yet",
      textCode: "cooldownHasntPassedYet",
    });
  }
}

/**
 * @apiDefine UsernameIsAlreadyUsedError
 *
 * @apiErrorExample UsernameIsAlreadyUsedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "Username is already used",
 *        "textCode": "usernameIsAlreadyUsed"
 *     }],
 *   }
 *
 */
class UsernameIsAlreadyUsedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.CONFLICT,
      message: "Username is already used",
      textCode: "usernameIsAlreadyUsed",
    });
  }
}

/**
 * @apiDefine UserIsAlreadyRegisteredError
 *
 * @apiErrorExample UserIsAlreadyRegisteredError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "User is already registered",
 *        "textCode": "userIsAlreadyRegistered"
 *     }],
 *   }
 *
 */
class UserIsAlreadyRegisteredError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "User is already registered",
      textCode: "userIsAlreadyRegistered",
    });
  }
}

/**
 * @apiDefine PhoneIsAlreadyUsedError
 *
 * @apiErrorExample PhoneIsAlreadyUsedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "Phone is already used",
 *        "textCode": "phoneIsAlreadyUsed"
 *     }],
 *   }
 *
 */
class PhoneIsAlreadyUsedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.CONFLICT,
      message: "Phone is already used",
      textCode: "phoneIsAlreadyUsed",
    });
  }
}

/**
 * @apiDefine EmailIsAlreadyUsedError
 *
 * @apiErrorExample EmailIsAlreadyUsedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "Email is already used",
 *        "textCode": "emailIsAlreadyUsed"
 *     }],
 *   }
 *
 */
class EmailIsAlreadyUsedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.CONFLICT,
      message: "Email is already used",
      textCode: "emailIsAlreadyUsed",
    });
  }
}

/**
 * @apiDefine PhoneOrEmailIsRequiredError
 *
 * @apiErrorExample PhoneOrEmailIsRequiredError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Phone or Email is required",
 *        "textCode": "phoneOrEmailIsRequired"
 *     }],
 *   }
 *
 */
class PhoneOrEmailIsRequiredError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Phone or Email is required",
      textCode: "phoneOrEmailIsRequired",
    });
  }
}

/**
 * @apiDefine UserNotFoundError
 *
 * @apiErrorExample UserNotFoundError:
 *   HTTP/1.1 404 NotFound
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 404,
 *        "message": "User not found",
 *        "textCode": "userNotFound"
 *      }]
 *   }
 *
 */
class UserNotFoundError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.NOT_FOUND,
      message: "User not found",
      textCode: "userNotFound",
    });
  }
}

/**
 * @apiDefine WrongEmailOrPasswordError
 *
 * @apiErrorExample WrongEmailOrPasswordError:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 401,
 *          "message": "Wrong email or password",
 *          "textCode": "wrongEmailOrPassword"
 *        }]
 *     }
 */
class WrongEmailOrPasswordError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.UNAUTHORIZED,
      message: "Wrong email or password",
      textCode: "wrongEmailOrPassword",
    });
  }
}

/**
 * @apiDefine WrongPhoneOrPasswordError
 *
 * @apiErrorExample WrongPhoneOrPasswordError:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 401,
 *          "message": "Wrong phone or password",
 *          "textCode": "wrongPhoneOrPassword"
 *        }]
 *     }
 */
class WrongPhoneOrPasswordError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.UNAUTHORIZED,
      message: "Wrong phone or password",
      textCode: "wrongPhoneOrPassword",
    });
  }
}

/**
 * @apiDefine WrongCodeError
 *
 * @apiErrorExample WrongCodeError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Wrong Email code",
 *        "textCode": "wrongEmailCode"
 *     }],
 *   }
 *
 */
class WrongCodeError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong code",
      textCode: "wrongCode",
    });
  }
}

/**
 * @apiDefine InvalidPasswordError
 *
 * @apiErrorExample InvalidPasswordError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *         "statusCode": 403,
 *          "message": "Invalid old password",
 *          "textCode": "invalidPassword"
 *        }]
 *     }
 */
class InvalidPasswordError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Invalid old password",
      textCode: "invalidPassword",
    });
  }
}

/**
 * @apiDefine WrongIdFormatError
 *
 * @apiErrorExample WrongIdFormatError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Wrong id format",
 *        "textCode": "wrongIdFormat"
 *      }]
 *   }
 *
 */
class WrongIdFormatError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong id format",
      textCode: "wrongIdFormat",
    });
  }
}

/**
 * @apiDefine WrongUsernameFormatError
 *
 * @apiErrorExample WrongUsernameFormatError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Wrong username format",
 *        "textCode": "wrongUsernameFormat"
 *      }]
 *   }
 *
 */
class WrongUsernameFormatError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong username format",
      textCode: "wrongUsernameFormat",
    });
  }
}

/**
 * @apiDefine NotEnoughCoinsError
 *
 * @apiErrorExample NotEnoughCoinsError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 403,
 *          "message": "Not enough coins",
 *          "textCode": "notEnoughCoins"
 *        }]
 *     }
 */
class NotEnoughCoinsError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Not enough coins",
      textCode: "notEnoughCoins",
    });
  }
}

/**
 * @apiDefine UserAccountIsBlockedError
 *
 * @apiErrorExample UserAccountIsBlockedError:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 401,
 *          "message": "User account is blocked"
 *          "textCode": "userAccountIsBlocked"
 *        }]
 *     }
 */
class UserAccountIsBlockedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.UNAUTHORIZED,
      message: "User account is blocked",
      textCode: "userAccountIsBlocked",
    });
  }
}

/**
 * @apiDefine UserShouldSetNewPasswordError
 *
 * @apiErrorExample UserShouldSetNewPasswordError:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 403,
 *          "message": "Verify reset password code first",
 *          "textCode": "userShouldSetNewPassword"
 *        }]
 *     }
 */
class UserShouldSetNewPasswordError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.FORBIDDEN,
      message: "Verify reset password code first",
      textCode: "userShouldSetNewPassword",
    });
  }
}

/**
 * @apiDefine AvatarChangeError
 *
 * @apiErrorExample AvatarChangeError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Avatar is required",
 *        "textCode": "avatarChangeError"
 *      }]
 *   }
 *
 */
class AvatarChangeError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Avatar is required",
      textCode: "avatarChangeError",
    });
  }
}

/**
 * @apiDefine EmailIsAlreadyVerifiedError
 *
 * @apiErrorExample EmailIsAlreadyVerifiedError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Email is alredy verified",
 *        "textCode": "emailIsAlreadyVerified"
 *      }]
 *   }
 *
 */
class EmailIsAlreadyVerifiedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Email is alredy verified",
      textCode: "emailIsAlreadyVerified",
    });
  }
}

/**
 * @apiDefine PhoneIsAlreadyVerifiedError
 *
 * @apiErrorExample PhoneIsAlreadyVerifiedError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Phone is alredy verified",
 *        "textCode": "phoneIsAlreadyVerified"
 *      }]
 *   }
 *
 */
class PhoneIsAlreadyVerifiedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Phone is alredy verified",
      textCode: "phoneIsAlreadyVerified",
    });
  }
}

/**
 * @apiDefine WrongPhoneNumberFormatError
 *
 * @apiErrorExample WrongPhoneNumberFormatError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 400,
 *        "message": "Wrong id format",
 *        "textCode": "wrongIdFormat"
 *      }]
 *   }
 *
 */
class WrongPhoneNumberFormatError extends ApiError {
  constructor(phone) {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: `${phone} is not a valid phone number`,
      textCode: "wrongPhoneNumberFormat",
    });
  }
}

/**
 * @apiDefine AlreadyFollowedError
 *
 * @apiErrorExample AlreadyFollowedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "You have already followed this user",
 *        "textCode": "alreadyFollowed"
 *      }]
 *   }
 *
 */
class AlreadyFollowedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.CONFLICT,
      message: "You have already followed this user",
      textCode: "alreadyFollowed",
    });
  }
}

/**
 * @apiDefine AlreadyUnfollowedError
 *
 * @apiErrorExample AlreadyUnfollowedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "You have already unfollowed this user",
 *        "textCode": "alreadyUnfollowed"
 *      }]
 *   }
 *
 */
class AlreadyUnfollowedError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.CONFLICT,
      message: "You have already unfollowed this user",
      textCode: "alreadyUnfollowed",
    });
  }
}

/**
 * @apiDefine WrongFilterError
 *
 * @apiErrorExample WrongFilterError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 400,
 *          "message": "Wrong filter",
 *          "textCode": "wrongFilter"
 *        }]
 *     }
 */
class WrongFilterError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong filter",
      textCode: "wrongFilter",
    });
  }
}

/**
 * @apiDefine WrongQueryFormatError
 *
 * @apiErrorExample WrongQueryFormatError:
 *   HTTP/1.1 400 BadRequest
 *   {
 *       "status": "failed",
 *       "result": {},
 *       "errors": [{
 *          "statusCode": 400,
 *          "message": "Wrong query format",
 *          "textCode": "WrongQueryFormat"
 *        }]
 *     }
 */
class WrongQueryFormatError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong query format",
      textCode: "WrongQueryFormat",
    });
  }
}

/**
 * @apiDefine EmailIsAlreadyUsedError
 *
 * @apiErrorExample EmailIsAlreadyUsedError:
 *   HTTP/1.1 409 Conflict
 *   {
 *     "status": "failed",
 *     "result": {},
 *     "errors": [{
 *        "statusCode": 409,
 *        "message": "Email is already used",
 *        "textCode": "emailIsAlreadyUsed"
 *     }],
 *   }
 *
 */

module.exports = {
  CooldownError,
  UsernameIsAlreadyUsedError,
  UserIsAlreadyRegisteredError,
  PhoneIsAlreadyUsedError,
  EmailIsAlreadyUsedError,
  PhoneOrEmailIsRequiredError,
  UserNotFoundError,
  WrongEmailOrPasswordError,
  WrongPhoneOrPasswordError,
  WrongCodeError,
  InvalidPasswordError,
  WrongIdFormatError,
  WrongUsernameFormatError,
  NotEnoughCoinsError,
  UserAccountIsBlockedError,
  UserShouldSetNewPasswordError,
  AvatarChangeError,
  EmailIsAlreadyVerifiedError,
  PhoneIsAlreadyVerifiedError,
  WrongPhoneNumberFormatError,
  AlreadyFollowedError,
  AlreadyUnfollowedError,
  WrongFilterError,
  WrongQueryFormatError,
};
