const { StatusCodes } = require("http-status-codes");
const { ApiError } = require("../../utils/api");

class WrongFileFormatError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong file format",
      textCode: "wrongFileFormat",
    });
  }
}
class WrongRangeFormatError extends ApiError {
  constructor() {
    super({
      statusCode: StatusCodes.BAD_REQUEST,
      message: "Wrong range header format",
      textCode: "wrongRangeFormat",
    });
  }
}

module.exports = {
  WrongFileFormatError,
  WrongRangeFormatError,
};
