const { getOrders } = require("./get");
const Order = require("./order.model");
const Users = require("../users");

async function serialize(order) {
  const { manager: managerId } = order;
  const manager = await Users.findById(managerId);
  return { ...order.toDTO(), manager: await Users.serialize(manager) };
}

async function serializeMany(orders) {
  return await Promise.all(
    orders.map(async function (o) {
      return await serialize(o);
    })
  );
}

module.exports = { getOrders, serialize, serializeMany };
