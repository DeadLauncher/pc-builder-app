const mongoose = require("mongoose");
const { logger } = require("../logger");
const orderStatuses = require("./orderStatuses");

const { ObjectId } = mongoose.Types;

const Order = mongoose.Schema({
  build: { type: ObjectId, ref: "Build" },
  manager: { type: ObjectId, ref: "User" },
  status: { type: String, default: orderStatuses.Created, required: true },
});

Order.methods.toDTO = function () {
  return {
    id: this.id,
    build: this.build,
    manager: this.manager,
    status: this.status,
  };
};

const model = mongoose.model("Order", Order);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
