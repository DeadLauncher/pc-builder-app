const Order = require("./order.model");

async function getOrders() {
  return await Order.find({});
}

module.exports = { getOrders };
