const { isValidPassword } = require("./crypto");
const User = require("./user.model");
const { WrongEmailOrPasswordError } = require("../errors/users");

const signIn = async ({ username, password }, session) => {
  const existingUser = await User.findOne({ login: username }).session(session);
  const userEnteredInvalidPassword =
    existingUser?.salt && !isValidPassword({ existingUser, password });
  if (!existingUser || userEnteredInvalidPassword)
    throw new WrongEmailOrPasswordError();
  return existingUser;
};

module.exports = { signIn };
