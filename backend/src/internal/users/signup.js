const { getPasswordInfo } = require("./crypto");
const User = require("./user.model");

const signUp = async ({ username, password }, session) => {
  const existingUser = await User.findOne({ username }).session(session);
  if (existingUser) {
    throw new UsernameIsAlreadyUsedError();
  }
  const { sessionKey, encryptedPassword, salt } = getPasswordInfo(password);
  const [user] = await User.create(
    [{ login: username, sessionKey, encryptedPassword, salt }],
    { session }
  );
  return user;
};

module.exports = { signUp };
