const crypto = require("crypto");

const encryptPassword = (password, salt) =>
  crypto.pbkdf2Sync(password, salt, 1, 128, "sha512").toString("hex");

const getPasswordInfo = (password) => {
  const sessionKey = crypto.randomBytes(128).toString("base64");
  const salt = crypto.randomBytes(128).toString("base64");
  const encryptedPassword = encryptPassword(password, salt);
  return { sessionKey, encryptedPassword, salt };
};

const isValidPassword = ({ existingUser, password }) =>
  encryptPassword(password, existingUser.salt) ===
  existingUser.encryptedPassword;

module.exports = { encryptPassword, getPasswordInfo, isValidPassword };
