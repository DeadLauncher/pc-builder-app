const mongoose = require("mongoose");
const { logger } = require("../logger");
const userRoles = require("./userRoles");

const { ObjectId } = mongoose.Types;

const userIdentifierParams = {
  index: true,
  trim: true,
  lowercase: true,
  unique: true,
  sparse: true,
};

const User = mongoose.Schema({
  login: { type: String, ...userIdentifierParams },
  name: { type: String },
  orders: [{ type: ObjectId, ref: "Order" }],
  encryptedPassword: { type: String },
  sessionKey: { type: String },
  salt: { type: String },
  role: { type: String, default: userRoles.Customer },
});

User.methods.toDTO = function () {
  const orders = this.orders.length ? this.orders : undefined;
  return {
    id: this.id,
    login: this.login,
    name: this.name || "",
    orders,
    role: this.role,
  };
};

const model = mongoose.model("User", User);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
