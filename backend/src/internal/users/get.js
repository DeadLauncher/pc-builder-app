const User = require("./user.model");

async function getUsers() {
  return await User.find({});
}

module.exports = { getUsers };
