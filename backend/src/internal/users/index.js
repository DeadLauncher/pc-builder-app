const { getUsers } = require("./get");
const { getUserById: findById } = require("./get-by-id");
const { signUp } = require("./signup");
const { signIn } = require("./signin");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(user) {
  return await user.toDTO();
}

async function serializeMany(users) {
  return await Promise.all(
    users.map(async function (u) {
      return await serialize(u);
    })
  );
}

module.exports = {
  getUsers,
  findById,
  serialize,
  serializeMany,
  signUp,
  signIn,
  findByIdWT: withTransaction(findById),
};
