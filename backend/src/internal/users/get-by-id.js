const User = require("./user.model");

async function getUserById(id) {
  return await User.findById(id);
}

module.exports = { getUserById };
