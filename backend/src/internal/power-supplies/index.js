const { getPowerSupplies } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(powerSupply) {
  return await powerSupply.toDTO();
}

async function serializeMany(powerSupplies) {
  return await Promise.all(
    powerSupplies.map(async function (ps) {
      return await serialize(ps);
    })
  );
}

module.exports = {
  getPowerSupplies,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
