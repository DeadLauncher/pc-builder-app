const PowerSupply = require("./power-supply.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    power,
    formfactor,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [powerSupply] = await PowerSupply.create(
    [
      {
        name,
        brand,
        price,
        weight,
        power,
        formfactor,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return powerSupply;
};

module.exports = { insert };
