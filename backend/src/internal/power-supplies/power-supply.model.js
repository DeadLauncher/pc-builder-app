const mongoose = require("mongoose");
const { logger } = require("../logger");

const PowerSupply = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  power: { type: Number },
  formfactor: { type: String },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

PowerSupply.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    power: this.power,
    formfactor: this.formfactor,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("PowerSupply", PowerSupply);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
