const PowerSupply = require("./power-supply.model");

async function getPowerSupplies() {
  return await PowerSupply.find({});
}

module.exports = { getPowerSupplies };
