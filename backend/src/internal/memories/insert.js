const Memory = require("./memory.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const { name, brand, price, weight, type, frequency, amount, image } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [memory] = await Memory.create(
    [
      {
        name,
        brand,
        price,
        weight,
        type,
        frequency,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return memory;
};

module.exports = { insert };
