const Memory = require("./memory.model");

async function getMemories() {
  return await Memory.find({});
}

module.exports = { getMemories };
