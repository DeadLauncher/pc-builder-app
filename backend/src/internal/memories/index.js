const { getMemories } = require("./get");
const { insert } = require("./insert");

const { withTransaction } = require("../../utils/with-transaction");

async function serialize(memory) {
  return await memory.toDTO();
}

async function serializeMany(memories) {
  return await Promise.all(
    memories.map(async function (ps) {
      return await serialize(ps);
    })
  );
}

module.exports = {
  getMemories,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
