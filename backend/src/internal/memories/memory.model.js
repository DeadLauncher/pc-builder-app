const mongoose = require("mongoose");
const { logger } = require("../logger");

const Memory = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  type: { type: String },
  frequency: { type: Number },
  capacity: { type: Number },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Memory.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    type: this.type,
    frequency: this.frequency,
    capacity: this.capacity,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Memory", Memory);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
