const Gpu = require("./gpu.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    power,
    memoryCapacity,
    memoryFrequency,
    coreFrequency,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [gpu] = await Gpu.create(
    [
      {
        name,
        brand,
        price,
        weight,
        power,
        memoryCapacity,
        memoryFrequency,
        coreFrequency,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return gpu;
};

module.exports = { insert };
