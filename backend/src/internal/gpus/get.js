const Gpu = require("./gpu.model");

async function getGpus() {
  return await Gpu.find({});
}

module.exports = { getGpus };
