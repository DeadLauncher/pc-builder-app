const { getGpus } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(gpu) {
  return await gpu.toDTO();
}

async function serializeMany(gpus) {
  return await Promise.all(
    gpus.map(async function (ps) {
      return await serialize(ps);
    })
  );
}

module.exports = {
  getGpus,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
