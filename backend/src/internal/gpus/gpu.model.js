const mongoose = require("mongoose");
const { logger } = require("../logger");

const Gpu = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  power: { type: Number },
  memoryCapacity: { type: Number },
  memoryFrequency: { type: Number },
  coreFrequency: { type: Number },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Gpu.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    power: this.power,
    memoryCapacity: this.memoryCapacity,
    memoryFrequency: this.memoryFrequency,
    coreFrequency: this.coreFrequency,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Gpu", Gpu);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
