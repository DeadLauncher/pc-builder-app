const Cpu = require("./cpu.model");
const { uploadFile } = require("../files");

const insert = async (props, me, session) => {
  const {
    name,
    brand,
    price,
    weight,
    power,
    frequency,
    socket,
    cores,
    graphics,
    amount,
    image,
  } = props;

  const imageUrl = image
    ? await uploadFile("image", image, "image")
    : undefined;

  const [cpu] = await Cpu.create(
    [
      {
        name,
        brand,
        price,
        weight,
        power,
        frequency,
        socket,
        cores,
        graphics,
        amount,
        image: imageUrl,
      },
    ],
    { session }
  );

  return cpu;
};

module.exports = { insert };
