const mongoose = require("mongoose");
const { logger } = require("../logger");

const Cpu = mongoose.Schema({
  name: { type: String },
  brand: { type: String },
  price: { type: Number },
  weight: { type: Number },
  power: { type: Number },
  frequency: { type: Number },
  socket: { type: String },
  cores: { type: Number },
  graphics: { type: Boolean },
  amount: { type: Number, default: 0 },
  image: { type: String },
});

Cpu.methods.toDTO = function () {
  return {
    id: this.id,
    name: this.name || "",
    brand: this.brand,
    price: this.price,
    weight: this.weight,
    power: this.power,
    frequency: this.frequency,
    socket: this.socket,
    cores: this.cores,
    graphics: this.graphics,
    amount: this.amount,
    image: this.image,
  };
};

const model = mongoose.model("Cpu", Cpu);

model.createCollection().then((collection) => {
  logger.info(`Collection "${collection.name}" created!`);
});

module.exports = model;
