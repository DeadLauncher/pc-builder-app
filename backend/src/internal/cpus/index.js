const { getCpus } = require("./get");
const { insert } = require("./insert");
const { withTransaction } = require("../../utils/with-transaction");

async function serialize(cpu) {
  return await cpu.toDTO();
}

async function serializeMany(cpus) {
  return await Promise.all(
    cpus.map(async function (c) {
      return await serialize(c);
    })
  );
}

module.exports = {
  getCpus,
  insert,
  insertWT: withTransaction(insert),
  serialize,
  serializeMany,
};
