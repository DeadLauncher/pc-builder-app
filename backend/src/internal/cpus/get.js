const Cpu = require("./cpu.model");

async function getCpus() {
  return await Cpu.find({});
}

module.exports = { getCpus };
