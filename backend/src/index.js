const process = require("process");
const engine = require("./engine");
const { logger } = require("./internal/logger");

(async () => {
  try {
    await engine.run();
  } catch (e) {
    logger.error(`engine error: ${e}`);
    process.exit();
  }
})();
