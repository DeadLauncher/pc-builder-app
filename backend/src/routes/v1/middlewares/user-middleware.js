const { handleError } = require("../../../utils/api");
const { WrongTokenError } = require("../../../internal/errors/middlewares");

const userMiddleware = (req, res, next) => {
  try {
    if (!req.user) {
      throw new WrongTokenError();
    }
    next();
  } catch (e) {
    handleError(res, e, next);
  }
};

module.exports = userMiddleware;
