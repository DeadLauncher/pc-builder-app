const passport = require("passport");
const Users = require("../../../internal/users");
const { Strategy } = require("passport-jwt");
const config = require("../../../config");

const tokenExtractor = (req) => {
  const token = req && req.cookies && req.cookies.accessToken;
  if (!token) {
    if (!req || !req.headers) {
      return null;
    }
    const { authorization } = req.headers;
    if (!authorization) {
      return null;
    }
    return authorization.split(" ")[1];
  }
  return token;
};

const options = {
  secretOrKey: config.users.auth.jwtSecret,
  jwtFromRequest: tokenExtractor,
};

passport.use(
  new Strategy(options, async (jwtPayload, callback) => {
    const user = await Users.findByIdWT(jwtPayload.id);
    const isSessionKeyRight = user?.sessionKey === jwtPayload.sessionKey;
    return isSessionKeyRight ? callback(null, user) : callback(null, null);
  })
);

const auth = (req, res, next) => {
  const authCallback = (error, user) => {
    req.user = user;
    next();
  };
  return passport.authenticate("jwt", { session: false }, authCallback)(
    req,
    res,
    next
  );
};

module.exports = auth;
