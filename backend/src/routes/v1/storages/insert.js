const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Storages = require("../../../internal/storages");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertStorage = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const storage = await Storages.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      storage: await Storages.serialize(storage, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  type: {
    exists: { errorMessage: "Type is required" },
  },
  capacity: {
    exists: { errorMessage: "Capacity is required" },
    isInt: {
      errorMessage: "Wrong capacity",
      options: { gt: 0 },
    },
  },
  spindleSpeed: {
    optional: {
      isInt: {
        errorMessage: "Wrong spindle speed",
        options: { gt: 0 },
      },
    },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertStorage
    );
};

module.exports = addEndpoint;
