const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Storages = require("../../../internal/storages");

const getStorages = async (req, res, next) => {
  try {
    const storages = await Storages.getStorages();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      storages: await Storages.serializeMany(storages),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getStorages);
};

module.exports = addEndpoint;
