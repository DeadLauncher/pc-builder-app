const express = require("express");

const config = require("../../../config");

const addGetStoragesEndpoint = require("./get");
const addInsertStorageEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetStoragesEndpoint(router);
  addInsertStorageEndpoint(router);

  app.use(`${config.api.root}/storages`, router);
};

module.exports = { registerEndpoints };
