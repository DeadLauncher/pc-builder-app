const express = require("express");

const config = require("../../../config");

const addGetGpusEndpoint = require("./get");
const addInsertGpuEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetGpusEndpoint(router);
  addInsertGpuEndpoint(router);

  app.use(`${config.api.root}/gpus`, router);
};

module.exports = { registerEndpoints };
