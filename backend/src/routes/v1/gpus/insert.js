const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Gpus = require("../../../internal/gpus");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertGpu = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const gpu = await Gpus.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      gpu: await Gpus.serialize(gpu, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  power: {
    exists: { errorMessage: "Power is required" },
    isInt: {
      errorMessage: "Wrong power",
      options: { gt: 0 },
    },
  },
  memoryCapacity: {
    exists: { errorMessage: "Memory capacity is required" },
    isInt: {
      errorMessage: "Wrong memory capacity",
      options: { gt: 0 },
    },
  },
  memoryFrequency: {
    exists: { errorMessage: "Memory frequency is required" },
    isInt: {
      errorMessage: "Wrong memory frequency",
      options: { gt: 0 },
    },
  },
  coreFrequency: {
    exists: { errorMessage: "Core frequency is required" },
    isInt: {
      errorMessage: "Wrong core frequency",
      options: { gt: 0 },
    },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertGpu
    );
};

module.exports = addEndpoint;
