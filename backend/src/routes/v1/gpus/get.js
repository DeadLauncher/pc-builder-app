const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Gpus = require("../../../internal/gpus");

const getGpus = async (req, res, next) => {
  try {
    const gpus = await Gpus.getGpus();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      gpus: await Gpus.serializeMany(gpus),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getGpus);
};

module.exports = addEndpoint;
