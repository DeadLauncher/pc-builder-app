const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const PowerSupplies = require("../../../internal/power-supplies");

const getPowerSupplies = async (req, res, next) => {
  try {
    const powerSupplies = await PowerSupplies.getPowerSupplies();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      powerSupplies: await PowerSupplies.serializeMany(powerSupplies),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getPowerSupplies);
};

module.exports = addEndpoint;
