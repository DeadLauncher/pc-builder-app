const express = require("express");

const config = require("../../../config");

const addGetPowerSuppliesEndpoint = require("./get");
const addInsertPowerSupplieEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetPowerSuppliesEndpoint(router);
  addInsertPowerSupplieEndpoint(router);

  app.use(`${config.api.root}/power-supplies`, router);
};

module.exports = { registerEndpoints };
