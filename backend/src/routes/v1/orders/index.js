const express = require("express");

const config = require("../../../config");

const addGetOrdersEndpoint = require("./get");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetOrdersEndpoint(router);

  app.use(`${config.api.root}/orders`, router);
};

module.exports = { registerEndpoints };
