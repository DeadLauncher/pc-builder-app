const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Orders = require("../../../internal/orders");

const getOrders = async (req, res, next) => {
  try {
    const orders = await Orders.getOrders();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      orders: await Orders.serializeMany(orders),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getOrders);
};

module.exports = addEndpoint;
