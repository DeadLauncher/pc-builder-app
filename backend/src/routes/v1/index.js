const registerUsersRouter = require("./users").registerEndpoints;
const registerOrdersRouter = require("./orders").registerEndpoints;
const registerCasesRouter = require("./cases").registerEndpoints;
const registerCpusRouter = require("./cpus").registerEndpoints;
const registerGpusRouter = require("./gpus").registerEndpoints;
const registerMemoriesRouter = require("./memories").registerEndpoints;
const registerMotherboardsRouter = require("./motherboards").registerEndpoints;
const registerStoragesRouter = require("./storages").registerEndpoints;
const registerBuildsRouter = require("./builds").registerEndpoints;
const registerPowerSuppliesRouter = require("./power-supplies")
  .registerEndpoints;

const registerApplicationEndpoints = (app) => {
  registerUsersRouter(app);
  registerOrdersRouter(app);
  registerCasesRouter(app);
  registerCpusRouter(app);
  registerGpusRouter(app);
  registerMemoriesRouter(app);
  registerMotherboardsRouter(app);
  registerStoragesRouter(app);
  registerBuildsRouter(app);
  registerPowerSuppliesRouter(app);
};

module.exports = {
  registerApplicationEndpoints,
};
