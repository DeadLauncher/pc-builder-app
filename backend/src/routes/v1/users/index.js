const express = require("express");

const config = require("../../../config");

const addGetUsersEndpoint = require("./get-list");
const addGetUserEndpoint = require("./get-by-id");
const addSignUpEndpoint = require("./signup");
const addSignInEndpoint = require("./signin");
const addGetMeEndpoint = require("./me");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetUsersEndpoint(router);
  addGetUserEndpoint(router);
  addSignUpEndpoint(router);
  addSignInEndpoint(router);
  addGetMeEndpoint(router);

  app.use(`${config.api.root}/users`, router);
};

module.exports = { registerEndpoints };
