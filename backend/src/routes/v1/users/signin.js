const { StatusCodes } = require("http-status-codes");
const authMiddleware = require("../middlewares/auth-middleware");
const { checkSchema } = require("express-validator");

const { handleError } = require("../../../utils/api");
const Users = require("../../../internal/users");
const { withTransaction } = require("../../../utils/with-transaction");
const validate = require("../../../utils/validate");
const userMiddleware = require("../middlewares/user-middleware");
const { getAuthenticatedUserResponse } = require("./common");
//const { UserNotFoundError } = require("../../../internal/errors/users");

const signIn = async (req, res, next) => {
  try {
    await withTransaction(async (session) => {
      const user = await Users.signIn({ ...req.body, ...req.params }, session);
      res
        .status(StatusCodes.OK)
        .json(await getAuthenticatedUserResponse(req, res, user));
    })();
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/signin").post(signIn);
};

module.exports = addEndpoint;
