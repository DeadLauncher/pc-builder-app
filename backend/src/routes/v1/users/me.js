const { StatusCodes } = require("http-status-codes");
const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Users = require("../../../internal/users");
const userMiddleware = require("../middlewares/user-middleware");
//const { withTransaction } = require("../../../utils/with-transaction");
//const { UserNotFoundError } = require("../../../internal/errors/users");

const getMe = async (req, res, next) => {
  try {
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      user: await Users.serialize(req.user),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(authMiddleware, userMiddleware, getMe);
};

module.exports = addEndpoint;
