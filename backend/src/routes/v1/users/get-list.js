const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Users = require("../../../internal/users");
//const { withTransaction } = require("../../../utils/with-transaction");
//const { UserNotFoundError } = require("../../../internal/errors/users");

/**
 * @api {get} /rest/v1/users
 * @apiName getUsers
 * @apiGroup Users
 * @apiPermission none
 * @apiDescription Return users.
 *
 * @apiSuccess {[Users](#api-Types-ObjectUser)} user user
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 * "status": "success",
 * "result": {
 * "user": {
 * "id": "5fcf...",
 * "email": "lol@kek.com",
 * },
 * },
 * "errors": []
 * }
 *
 * @apiUse UserNotFoundError
 * @apiUse WrongIdFormatError
 * @apiUse InternalServerError
 */

const getUsers = async (req, res, next) => {
  try {
    const users = await Users.getUsers();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      users: await Users.serializeMany(users),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/list").get(getUsers);
};

module.exports = addEndpoint;
