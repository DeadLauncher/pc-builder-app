const Users = require("../../../internal/users");
const config = require("../../../config");
const { generateAccessToken } = require("../../../internal/token");

const {
  formatResponse,
  RESPONSE_STATUS_FAILED,
  RESPONSE_STATUS_SUCCESS,
} = require("../../../utils/api");

const getAuthenticatedUserResponse = async (req, res, user) => {
  const accessToken = generateAccessToken(user);
  const maxAge = config.users.auth.accessTokenMaxAge;
  const serializedUser = await Users.serialize(user);
  let result;
  if (req.body.tokenInCookie || req.query.tokenInCookie) {
    result = { user: serializedUser };
    res.cookie("accessToken", accessToken, { maxAge, httpOnly: true });
  } else {
    result = { user: serializedUser, accessToken, maxAge };
  }
  return formatResponse(RESPONSE_STATUS_SUCCESS, result);
};

module.exports = { getAuthenticatedUserResponse };
