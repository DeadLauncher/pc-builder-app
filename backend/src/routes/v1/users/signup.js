const { StatusCodes } = require("http-status-codes");
const authMiddleware = require("../middlewares/auth-middleware");
const { checkSchema } = require("express-validator");

const { handleError } = require("../../../utils/api");
const Users = require("../../../internal/users");
const { withTransaction } = require("../../../utils/with-transaction");
const validate = require("../../../utils/validate");
const userMiddleware = require("../middlewares/user-middleware");
const { getAuthenticatedUserResponse } = require("./common");
//const { UserNotFoundError } = require("../../../internal/errors/users");

const validationSchema = checkSchema({
  username: {
    trim: true,
    exists: {
      errorMessage: "Username is required.",
    },
    isLength: {
      errorMessage: "Wrong username length.",
      options: {
        min: 5,
        max: 20,
      },
    },
  },
  password: {
    trim: true,
    isLength: {
      errorMessage: "Wrong password length.",
      options: {
        min: 5,
        max: 32,
      },
    },
    optional: {
      checkFalsy: true,
    },
  },
});

const signUp = async (req, res, next) => {
  try {
    await withTransaction(async (session) => {
      const user = await Users.signUp({ ...req.body, ...req.params }, session);
      res
        .status(StatusCodes.OK)
        .json(await getAuthenticatedUserResponse(req, res, user));
    })();
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router
    .route("/signup")
    .put(authMiddleware, [validationSchema, validate], signUp);
};

module.exports = addEndpoint;
