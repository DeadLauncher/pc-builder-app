const express = require("express");

const config = require("../../../config");

const addGetCpusEndpoint = require("./get");
const addInsertCpuEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetCpusEndpoint(router);
  addInsertCpuEndpoint(router);

  app.use(`${config.api.root}/cpus`, router);
};

module.exports = { registerEndpoints };
