const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Cpus = require("../../../internal/cpus");

const getCpus = async (req, res, next) => {
  try {
    const cpus = await Cpus.getCpus();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      cpus: await Cpus.serializeMany(cpus),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getCpus);
};

module.exports = addEndpoint;
