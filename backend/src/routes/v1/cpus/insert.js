const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Cpus = require("../../../internal/cpus");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertCpu = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const cpu = await Cpus.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      cpu: await Cpus.serialize(cpu, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  power: {
    exists: { errorMessage: "Power is required" },
    isInt: {
      errorMessage: "Wrong power",
      options: { gt: 0 },
    },
  },
  frequency: {
    exists: { errorMessage: "Frequency is required" },
    isInt: {
      errorMessage: "Wrong frequency",
      options: { gt: 0 },
    },
  },
  socket: {
    exists: { errorMessage: "Socket is required" },
    trim: true,
  },
  cores: {
    exists: { errorMessage: "Cores is required" },
    isInt: {
      errorMessage: "Wrong cores number",
      options: { gt: 0 },
    },
  },
  graphics: {
    exists: { errorMessage: "Graphics is required" },
    isBoolean: { errorMessage: "Wrong graphics value" },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertCpu
    );
};

module.exports = addEndpoint;
