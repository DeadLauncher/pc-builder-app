const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Memories = require("../../../internal/memories");

const getMemories = async (req, res, next) => {
  try {
    const memories = await Memories.getMemories();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      memories: await Memories.serializeMany(memories),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getMemories);
};

module.exports = addEndpoint;
