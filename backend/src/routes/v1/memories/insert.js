const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Memories = require("../../../internal/memories");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertMemory = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const memory = await Memories.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      memory: await Memories.serialize(memory, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  capacity: {
    exists: { errorMessage: "Capacity is required" },
    isInt: {
      errorMessage: "Wrong capacity",
      options: { gt: 0 },
    },
  },
  frequency: {
    exists: { errorMessage: "Frequency is required" },
    isInt: {
      errorMessage: "Wrong frequency",
      options: { gt: 0 },
    },
  },
  type: {
    exists: { errorMessage: "Type is required" },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertMemory
    );
};

module.exports = addEndpoint;
