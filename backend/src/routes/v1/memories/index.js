const express = require("express");

const config = require("../../../config");

const addGetMemoriesEndpoint = require("./get");
const addInsertMemoriesEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetMemoriesEndpoint(router);
  addInsertMemoriesEndpoint(router);

  app.use(`${config.api.root}/memories`, router);
};

module.exports = { registerEndpoints };
