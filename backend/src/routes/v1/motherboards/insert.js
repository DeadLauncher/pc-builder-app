const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Motherboards = require("../../../internal/motherboards");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertMotherboard = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const motherboard = await Motherboards.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      motherboard: await Motherboards.serialize(motherboard, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  formfactor: {
    exists: { errorMessage: "Formfactor is required" },
  },
  cpuSocket: {
    exists: { errorMessage: "CPU socket is required" },
  },
  memoryType: {
    exists: { errorMessage: "Memory type is required" },
  },
  memorySlots: {
    exists: { errorMessage: "Memory slots is required" },
    isInt: {
      errorMessage: "Wrong memory slots count",
      options: { gt: 0 },
    },
  },
  pciSlots: {
    exists: { errorMessage: "PCI slots is required" },
    isInt: {
      errorMessage: "Wrong PCI slots count",
      options: { gt: 0 },
    },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertMotherboard
    );
};

module.exports = addEndpoint;
