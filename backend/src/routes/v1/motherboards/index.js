const express = require("express");

const config = require("../../../config");

const addGetMotherboardsEndpoint = require("./get");
const addInsertMotherboardEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetMotherboardsEndpoint(router);
  addInsertMotherboardEndpoint(router);

  app.use(`${config.api.root}/motherboards`, router);
};

module.exports = { registerEndpoints };
