const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Motherboards = require("../../../internal/motherboards");

const getMotherboards = async (req, res, next) => {
  try {
    const motherboards = await Motherboards.getMotherboards();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      motherboards: await Motherboards.serializeMany(motherboards),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getMotherboards);
};

module.exports = addEndpoint;
