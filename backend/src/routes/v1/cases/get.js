const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Cases = require("../../../internal/cases");

const getCases = async (req, res, next) => {
  try {
    const cases = await Cases.getCases();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      cases: await Cases.serializeMany(cases),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getCases);
};

module.exports = addEndpoint;
