const express = require("express");

const config = require("../../../config");

const addGetCasesEndpoint = require("./get");
const addInsertCaseEndpoint = require("./insert");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetCasesEndpoint(router);
  addInsertCaseEndpoint(router);

  app.use(`${config.api.root}/cases`, router);
};

module.exports = { registerEndpoints };
