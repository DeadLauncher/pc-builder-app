const multer = require("multer");
const { StatusCodes } = require("http-status-codes");
const { checkSchema } = require("express-validator");

//const authMiddleware = require("../middlewares/auth-middleware");
//const userMiddleware = require("../middlewares/user-middleware");
//const registerMiddleware = require("../middlewares/register-middlevare");
//const adminMiddleware = require("../middlewares/admin-middleware");

const Cases = require("../../../internal/cases");
const { PartImageIsRequiredError } = require("../../../internal/errors/parts");
const wrapAsyncTransactionErrorMiddleware = require("../../../utils/middlewares/wrap-async-transaction-error-middleware");
const { sendResponse } = require("../../../utils/api");
const validate = require("../../../utils/validate");
const partsValidationSchema = require("../../../utils/validation-schemas/parts-validation-schema");

const insertCase = wrapAsyncTransactionErrorMiddleware(
  async (req, res, session) => {
    //if (!req?.files?.image || !req?.files?.image?.[0]) {
    //  throw new PartImageIsRequiredError();
    //}
    const casee = await Cases.insert(
      {
        ...req.query,
        ...req.body,
        image: req?.files?.image?.[0],
      },
      req.user,
      session
    );
    sendResponse(res, StatusCodes.OK, {
      casee: await Cases.serialize(casee, req.user, session),
    });
  }
);
const validationSchema = checkSchema({
  ...partsValidationSchema,
  size: {
    exists: { errorMessage: "Size is required" },
  },
  mbFormfactor: {
    exists: { errorMessage: "Motherboard formfactor is required" },
  },
  psFormfactor: {
    exists: { errorMessage: "Power supply formfactor is required" },
  },
});

const addEndpoint = (router) => {
  router
    .route("/insert")
    .post(
      multer().fields([{ name: "image", maxCount: 1 }]),
      [validationSchema, validate],
      insertCase
    );
};

module.exports = addEndpoint;
