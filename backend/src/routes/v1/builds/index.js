const express = require("express");

const config = require("../../../config");

const addGetBuildsEndpoint = require("./get");

const registerEndpoints = (app) => {
  const router = express.Router({ mergeParams: true });

  addGetBuildsEndpoint(router);

  app.use(`${config.api.root}/builds`, router);
};

module.exports = { registerEndpoints };
