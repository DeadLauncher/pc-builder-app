const { StatusCodes } = require("http-status-codes");
//const authMiddleware = require("../middlewares/auth-middleware");

const {
  RESPONSE_STATUS_SUCCESS,
  formatResponse,
  handleError,
} = require("../../../utils/api");
const Builds = require("../../../internal/builds");

const getBuilds = async (req, res, next) => {
  try {
    const builds = await Builds.getBuilds();
    const formatedResult = formatResponse(RESPONSE_STATUS_SUCCESS, {
      builds: await Builds.serializeMany(builds),
    });
    res.status(StatusCodes.OK).json(formatedResult);
  } catch (e) {
    handleError(res, e, next);
  }
};

const addEndpoint = (router) => {
  router.route("/").get(getBuilds);
};

module.exports = addEndpoint;
