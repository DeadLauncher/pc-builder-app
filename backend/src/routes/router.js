const bodyParser = require("body-parser");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const { StatusCodes } = require("http-status-codes");
const {
  expressRequestLogger,
  expressErrorLogger,
} = require("../internal/logger");
const { registerApplicationEndpoints } = require("./v1");

const config = require("../config");

const {
  RESPONSE_STATUS_FAILED,
  formatResponse,
  formatError,
} = require("../utils/api");

function applyPreRequestMiddlewares(app) {
  app.use(
    cors({
      origin: config.api.allowedOrigin,
      credentials: true,
      allowedHeaders: "X-Requested-With,Content-Type,withCredentials",
    })
  );
  app.use(cookieParser());
  app.use(bodyParser.json({ extended: true, limit: "4MB" }));
  app.use(passport.initialize({}));
  app.use(expressRequestLogger);
}

function applyPostRequestMiddlewares(app) {
  app.use(expressErrorLogger);
}

function addNotFoundRoute(app) {
  app.use((req, res) => {
    const error = formatError("Not found");
    const response = formatResponse(RESPONSE_STATUS_FAILED, {}, [error]);
    res.status(StatusCodes.NOT_FOUND).send(response);
  });
}

function applyRouter(app) {
  applyPreRequestMiddlewares(app);
  registerApplicationEndpoints(app);

  addNotFoundRoute(app);
  applyPostRequestMiddlewares(app);
}

module.exports = {
  applyRouter,
};
