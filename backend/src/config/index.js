module.exports = {
  api: {
    port: 3030,
    root: "/rest/v1",
  },
  mongo: {
    url: "mongodb://mongo:27017/pcbuilder",
  },
  logger: {
    enabled: true,
  },
  users: {
    auth: {
      jwtSecret: process.env.USERS_AUTH_JWT_SECRET,
      accessTokenMaxAge: 24 * 60 * 60 * 1000,
    },
  },
  google: {
    storage: {
      bucket: "pcbuilder-dev",
      timeout: 24 * 60 * 60 * 1000,
    },
  },
};
