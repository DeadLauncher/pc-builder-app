Mongo (в консоль):

```
docker exec -it pc-builder-app_mongo_1 mongo --eval "rs.initiate({ _id: 'rs0', members: [{_id: 0, host: 'mongo:27017'}] });"
```

Docker (в консоль):

```
docker-compose -f docker-compose.local.yml up --build -d --force-recreate
```

Файл .env

```
API_URL=http://backend:3030
API_PREFIX=rest/v1
API_TOKEN_IN_BODY=true
```
