import React, { useState } from "react";
import Link from "next/link";
import {
  Wrapper,
  HeaderWrapper,
  LogoWrapper,
  LogoLabel,
  FlexStub,
  BodyWrapper,
} from "./styles";

const Layout = ({ children }) => {
  return (
    <Wrapper>
      <HeaderWrapper>
        <LogoWrapper>
          <Link href="/portal">
            <LogoLabel>FUNITE</LogoLabel>
          </Link>
        </LogoWrapper>
        <FlexStub />
      </HeaderWrapper>
      <BodyWrapper>{children}</BodyWrapper>
    </Wrapper>
  );
};

export default Layout;
