import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
`;
const HeaderWrapper = styled.div`
  width: 100%;
`;
const LogoWrapper = styled.div`
  width: 100%;
`;
const LogoLabel = styled.span`
  width: 100%;
`;
const FlexStub = styled.div`
  width: 100%;
`;
const BodyWrapper = styled.div`
  width: 100%;
`;

export {
  Wrapper,
  HeaderWrapper,
  LogoWrapper,
  LogoLabel,
  FlexStub,
  BodyWrapper,
};
