import React, { useState } from "react";
import getUser from "../lib/users/get";

export const Context = React.createContext({});
export const AuthorizationProvider = (props) => {
  const { children, user: initialUser } = props;
  const [user, setUser] = useState(initialUser);
  return (
    <Context.Provider value={{ user, setUser }}>{children}</Context.Provider>
  );
};

export const getInitialAuthorizationProps = async (ctx) => {
  const response = await getUser("", ctx);
  return { user: response.data.result.user };
};
