import Router from "next/router";

const redirect = async (ctx, pathname) => {
  if (ctx.res) {
    ctx.res.writeHead(302, { Location: pathname });
    ctx.res.end();
  } else {
    await Router.push(pathname);
  }
};

export default redirect;
