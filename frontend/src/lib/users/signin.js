import request from "../api/request";

export default (data) => request("users/signin", { method: "post", data });
