import request from "../api/request";

export default (id, ctx) =>
  request(id ? `users/${id}` : "users", { method: "get" }, ctx);
