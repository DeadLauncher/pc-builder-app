import styled from "styled-components";

const PageWrapper = styled.div`
  width: 100%;
`;
const LoginForm = styled.form`
  width: 100%;
`;
const Input = styled.input`
  width: 100%;
`;
const SubmitButton = styled.button`
  width: 100%;
`;

export { PageWrapper, LoginForm, Input, SubmitButton };
