import React, { useEffect, useState } from "react";
import Router from "next/router";
import signin from "../../lib/users/signin";
import { PageWrapper, LoginForm, Input, SubmitButton } from "./styles";

const LoginPage = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const handleSubmit = async () => {
    const response = await signin({ username, password });
    if (response.data.status === "failed") {
      return;
    }
    await Router.push("/portal");
  };
  return (
    <PageWrapper>
      <LoginForm
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }}
      >
        <Input
          type="username"
          value={username}
          onChange={({ target }) => setUsername(target.value)}
        />
        <Input
          type="password"
          value={password}
          onChange={({ target }) => setPassword(target.value)}
        />
        <SubmitButton>Login</SubmitButton>
      </LoginForm>
    </PageWrapper>
  );
};

export default LoginPage;
