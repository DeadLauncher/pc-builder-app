import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Layout from "../src/components/layout/layout";

const Home = () => {
  const router = useRouter();
  useEffect(() => {
    router.push("/portal");
  }, [router]);
  return <Layout />;
};

export default Home;
