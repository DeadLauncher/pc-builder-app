import React from "react";
import Head from "next/head";
import {
  AuthorizationProvider,
  getInitialAuthorizationProps,
} from "../src/context/authorization";
import "./root.css";
import "./styles.css";
import redirect from "../src/lib/redirect";

const routeCheck = {
  "/login": async (ctx, user) => {
    if (user) {
      await redirect(ctx, "/portal");
    }
  },
};
const App = (props) => {
  const { Component, pageProps, authorizationProps } = props;
  return (
    <React.Fragment>
      <Head>
        {/* <link rel="icon" type="image/png" sizes="32x32" href="" /> */}
        <title>PC Builder</title>
      </Head>
      <AuthorizationProvider {...authorizationProps}>
        <Component {...pageProps} />
      </AuthorizationProvider>
    </React.Fragment>
  );
};

App.getInitialProps = async ({ Component, ctx }) => {
  const authorizationProps = await getInitialAuthorizationProps(ctx);
  if (!authorizationProps.user && ctx.pathname !== "/login") {
    await redirect(ctx, "/login");
  }
  const check = routeCheck[ctx.pathname];
  if (check) {
    await check(ctx, authorizationProps.user);
  }
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};
  return { pageProps, authorizationProps };
};

export default App;
