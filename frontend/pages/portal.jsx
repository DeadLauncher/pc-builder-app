import React from "react";
import PortalPage from "../src/features/portal/portal-page";
import Layout from "../src/components/layout/layout";

const Portal = () => (
  <Layout>
    <PortalPage />
  </Layout>
);

export default Portal;
