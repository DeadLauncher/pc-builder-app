import React from "react";

// TODO error page
const Error = ({ statusCode = 404 }) => {
  switch (+statusCode) {
    case 400:
    case 401:
    case 403:
    case 404:
    case 500:
      return <div>{statusCode}</div>;
    default:
      return <div>Unexpected error</div>;
  }
};

Error.getInitialProps = async ({ res, err }) => {
  const statusCode = res ? res.statusCode : err?.statusCode || 404;
  return { statusCode };
};

export default Error;
