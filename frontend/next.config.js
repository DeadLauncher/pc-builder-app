module.exports = {
  env: {
    apiUrl: process.env.NEXT_PUBLIC_API_URL || "http://backend:3030",
    apiPrefix: process.env.NEXT_PUBLIC_API_PREFIX || "rest/v1",
    apiTokenInBody: process.env.NEXT_PUBLIC_API_TOKEN_IN_BODY === "true",
  },
};
